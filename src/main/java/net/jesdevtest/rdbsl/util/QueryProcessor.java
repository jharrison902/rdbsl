package net.jesdevtest.rdbsl.util;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import net.jesdevtest.rdbsl.dto.QueryFilter;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

public class QueryProcessor {

  private static QueryProcessor queryProcessor;

  public static QueryProcessor getInstance() {
    if (queryProcessor == null) {
      queryProcessor = new QueryProcessor();
    }
    return queryProcessor;
  }

  /*
  Public for testing
   */
  public QueryProcessor() {
  }

  public List<Map<String, Object>> selectFromSchema(NamedParameterJdbcTemplate jdbcTemplate,
      final String schema, final String table,
      final Optional<List<QueryFilter>> filters) {

    final String sanitizedSchema = schema.replaceAll("[\\W]|_", "");
    final String sanitizedTable = table.replaceAll("[\\W]|_", "");
    if (sanitizedSchema.isEmpty() || sanitizedTable.isEmpty()) {
      return null;
    }
    final String baseQuery = String.format("SELECT * FROM %s.%s ", sanitizedSchema, sanitizedTable);
    if (filters.isPresent()) {
      final Map<String, Object> params = new HashMap<>();
      filters.get().stream().forEach(entry -> params.put(entry.getField(), entry.getValue()));
      final String clausedQuery = String.format("%s WHERE %s", baseQuery, String.join(" AND ",
          filters.get().stream().map(entry -> String
              .format("%s %s :%s", entry.getField().replaceAll("[\\W]|_", ""),
                  entry.getOperator().toString(),
                  entry.getField().replaceAll("[\\W]|_", ""))).collect(
              Collectors.toList())));
      return jdbcTemplate.query(clausedQuery, params, new DynamicRowMapper());
    } else {
      return jdbcTemplate.query(baseQuery, new DynamicRowMapper());
    }
  }

  private class DynamicRowMapper implements RowMapper<Map<String, Object>> {

    @Override
    public Map<String, Object> mapRow(ResultSet resultSet, int i) throws SQLException {
      final Map<String, Object> record = new HashMap<>();
      final ResultSetMetaData metaData = resultSet.getMetaData();

      final int bound = metaData.getColumnCount() + 1;
      for (int j = 1; j < bound; j++) {

        Object o;
        switch (metaData.getColumnType(j)) {
          case Types.VARCHAR:
          case Types.CHAR:
            o = resultSet.getString(j);
            break;
          case Types.INTEGER:
            o = resultSet.getInt(j);
            break;
          case Types.FLOAT:
          case Types.DOUBLE:
            o = resultSet.getFloat(j);
            break;
          case Types.DATE:
            o = resultSet.getDate(j);
            break;
          case Types.BIGINT:
            o = resultSet.getLong(j);
            break;
          case Types.DECIMAL:
            o = resultSet.getBigDecimal(j);
            break;
          case Types.BOOLEAN:
            o = resultSet.getBoolean(j);
            break;
          default:
            o = null;
            break;
        }
        record.put(metaData.getColumnName(j), o);
      }
      return record;
    }
  }
}

