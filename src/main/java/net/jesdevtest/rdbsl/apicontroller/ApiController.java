package net.jesdevtest.rdbsl.apicontroller;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import net.jesdevtest.rdbsl.dto.QueryFilter;
import net.jesdevtest.rdbsl.service.DatabaseAccessService;
import net.jesdevtest.rdbsl.util.QueryOperator;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class ApiController {

  private final DatabaseAccessService databaseAccessService;

  @Autowired
  public ApiController(DatabaseAccessService databaseAccessService) {
    this.databaseAccessService = databaseAccessService;
  }

  @RequestMapping(value = {"/test"}, method = RequestMethod.GET)
  public String testAndInitialize() {
    Flyway flyway = Flyway.configure().dataSource(
        "jdbc:h2:~/test", "sa",
        "").load();
    flyway.clean();
    flyway.migrate();
    return "OK";
  }

  @RequestMapping(value = {"/{schema}/{table}/{id}"}, method = RequestMethod.GET)
  public List<Map<String, Object>> getFromSchemaTable(
      @PathVariable(value = "schema") final String schema,
      @PathVariable(value = "table") final String table,
      @PathVariable(value = "id") final String id) {

    Optional<List<QueryFilter>> idFilter = Optional
        .of(Collections.singletonList(new QueryFilter("id", QueryOperator.EQ, id)));
    return databaseAccessService.selectFromSchema(schema, table, idFilter);
  }

  @RequestMapping(value = {"/{schema}/{table}"}, method = RequestMethod.GET)
  public List<Map<String, Object>> getFromSchemaTableWithQuery(
      @PathVariable(value = "schema") final String schema,
      @PathVariable(value = "table") final String table,
      @RequestParam(value = "query", required = false) final String query)
      throws IOException {

    return databaseAccessService
        .selectFromSchema(schema, table, Optional.ofNullable(query == null ? null : convertQueryFilters(query)));
  }

  /**
   * Avoid making a converter class for what's essentially a walled map.
   * @param rawCriteria
   * @return
   * @throws IOException
   */
  private List<QueryFilter> convertQueryFilters(String rawCriteria) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    List<Map<String, Object>> test = mapper.readValue(rawCriteria, ArrayList.class);
    return test.stream().map(entryMap -> new QueryFilter(entryMap.get("field").toString(),
        QueryOperator.valueOf(entryMap.get("operator").toString()),
        entryMap.get("value").toString())).collect(
        Collectors.toList());
  }
}
