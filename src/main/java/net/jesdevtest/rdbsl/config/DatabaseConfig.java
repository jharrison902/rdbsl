package net.jesdevtest.rdbsl.config;

import java.util.Optional;
import javax.sql.DataSource;
import org.pmw.tinylog.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * DatabaseConfig
 *
 * Contains the relevant data to generate a default database connection
 */
@Configuration
@ConfigurationProperties(prefix = "datasource")
public class DatabaseConfig {

  @Autowired
  private Environment env;

  @Bean(name = "defaultDatasource")
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(env.getRequiredProperty("datasource.driverClassName"));
    Logger.debug("Setting driver class name {}", env.getRequiredProperty("datasource.driverClassName"));
    dataSource.setUrl(env.getRequiredProperty("datasource.url"));
    Logger.debug("Setting url {}", env.getRequiredProperty("datasource.url"));
    Optional<String> username = Optional.ofNullable(env.getProperty("datasource.username"));
    Optional<String> blankUsername = Optional.ofNullable(env.getProperty("datasource.blankUsername"));
    Optional<String> password = Optional.ofNullable(env.getProperty("datasource.password"));
    Optional<String> blankPassword = Optional.ofNullable(env.getProperty("datasource.blankPassword"));
    if (blankUsername.isPresent()) {
      dataSource.setUsername("");
      Logger.debug("Setting username BLANK");
    } else {
      username.ifPresent(user -> dataSource.setUsername(user));
      Logger.debug("Setting username {}", username.get());
    }
    if (blankPassword.isPresent()) {
      dataSource.setPassword("");
      Logger.debug("Setting password BLANK");
    } else {
      password.ifPresent(pw -> dataSource.setPassword(pw));
      Logger.debug("Setting password {}", password.get());
    }

    return dataSource;
  }

  @Bean(name = "defaultNamedJdbc")
  public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource) {
    return new NamedParameterJdbcTemplate(dataSource);
  }

  @Bean(name = "defaultJdbc")
  public JdbcTemplate jdbcTemplate(DataSource dataSource) {
    return new JdbcTemplate(dataSource);
  }

}
