DROP TABLE IF EXISTS test.notes;
DROP TABLE IF EXISTS test.invoices;

DROP SCHEMA IF EXISTS test;
CREATE SCHEMA test;

CREATE TABLE test.notes (
id int NOT NULL,
title varchar(64) NOT NULL,
body varchar(256)
);

CREATE TABLE test.invoices (
id int NOT NULL,
amount numeric(10,2) DEFAULT 0.00,
comment varchar(256),
latest boolean DEFAULT TRUE
);